package com.ip.testingexamples;

import com.ip.testingexamples.domain.Email;
import com.ip.testingexamples.domain.Offer;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class ExceptionsExamples {
    private Offer offer;

    @Before
    public void before() {
        offer = new Offer("cff47c9c-29ff-4742-9535-8cd8a2dfa90e", 1);
    }


    @Test
    public void shouldVerifyExceptionMessageBeforeJunit4_7() {
        try {
            new Email(null);
            fail("Code did not throw exception");
        } catch (NullPointerException e) {
            assertEquals("No offer - no emails", e.getMessage());
        }
    }


    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void shouldVerifyExceptionMessageLaterJunit() {
        expectedException.expect(NullPointerException.class);
        expectedException.expectMessage("No offer - no emails");
        new Email(null);
    }

    @Test
    public void shouldVerifyExceptionMessageFailsJunit() {
        expectedException.expect(NullPointerException.class);
        expectedException.expectMessage("No offer - no emails");
        new Email(offer);
    }

    @Test
    public void shouldVerifyExceptionMessageAssertJ() {
        assertThatExceptionOfType(NullPointerException.class).isThrownBy(() -> new Email(null)).withMessage("No offer - no emails");
    }

    @Test
    public void shouldVerifyExceptionMessageAssertJSpecialisedException() {
        assertThatNullPointerException().isThrownBy(() -> new Email(null)).withMessage("No offer - no emails");
//        assertThatIOException()
//        assertThatIllegalArgumentException()
//        assertThatIllegalStateException()
    }

    @Test
    public void shouldVerifyExceptionMessageFailsAssertJ() {
        assertThatNullPointerException().isThrownBy(() -> new Email(offer)).withMessage("No offer - no emails");
    }
}