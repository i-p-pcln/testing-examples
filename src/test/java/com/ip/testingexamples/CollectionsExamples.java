package com.ip.testingexamples;

import com.ip.testingexamples.domain.Offer;
import org.hamcrest.Description;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class CollectionsExamples {
    private Offer offer1;
    private Offer offer5;
    private List<Offer> offers = new LinkedList<>();

    @Before
    public void before() {
        offer1 = new Offer("cff47c9c-29ff-4742-9535-8cd8a2dfa90e", 1);
        offer5 = new Offer("cff47c9c-29ff-4742-9535-8cd8a2dfa90e", 5);
        offers.add(offer1);
        offers.add(offer5);
    }

    @Test
    public void shouldContainInOrder() {
        List<Offer> expectedOffers = new LinkedList<>();
        expectedOffers.add(offer1);
        expectedOffers.add(offer5);

        // junit
        assertEquals(expectedOffers.size(), offers.size());
        for (int i = 0; i < expectedOffers.size(); i++) {
            assertEquals(expectedOffers.get(i), offers.get(i));
        }

        // hamcrest
        assertThat(offers, Matchers.contains(expectedOffers));

        // assertj
        assertThat(offers).containsExactlyElementsOf(expectedOffers);
    }

    @Test
    public void shouldContainInOrderFailsHamcrest() {
        List<Offer> expectedOffers = new LinkedList<>();
        expectedOffers.add(offer5);
        expectedOffers.add(offer1);

        assertThat(offers, Matchers.contains(expectedOffers));
    }

    @Test
    public void shouldContainInOrderFailsAssertJ() {
        List<Offer> expectedOffers = new LinkedList<>();
        expectedOffers.add(offer5);
        expectedOffers.add(offer1);

        assertThat(offers).containsExactlyElementsOf(expectedOffers);
    }

    @Test
    public void shouldNotContainDuplicates() {
        // junit
        assertEquals(offers.size(), offers.stream().distinct().collect(Collectors.toList()).size());

        // hamcrest
        // no matcher built-in, can be implemented with custom matcher
        assertThat(offers, hasNoDuplicates());

        // assertj
        assertThat(offers).doesNotHaveDuplicates();
    }

    private HasNoDuplicates<Offer> hasNoDuplicates() {
        return new HasNoDuplicates<>();
    }

    private class HasNoDuplicates<T> extends TypeSafeMatcher<List<T>> {
        private int distincElementsCount = 0;
        private int totalElementsCount = 0;

        @Override
        protected boolean matchesSafely(List<T> item) {
            if (item == null || item.size() < 2) {
                return true;
            } else {
                totalElementsCount = item.size();
                distincElementsCount = item.stream().distinct().collect(Collectors.toList()).size();
                return distincElementsCount == totalElementsCount;
            }
        }

        @Override
        public void describeTo(Description description) {
            description.appendText(String.format("%s distinct elements, but found %s duplicate(s)",
                    totalElementsCount, totalElementsCount - distincElementsCount));
        }
    }

    @Test
    public void shouldNotContainDuplicatesFailsHamcrest() {
        offers.add(offer1);
        assertThat(offers, hasNoDuplicates());
    }

    @Test
    public void shouldNotContainDuplicatesFailsAssertJ() {
        offers.add(offer1);
        assertThat(offers).doesNotHaveDuplicates();
    }


    @Test
    public void shouldNotContainNulls() {
        // junit
        for (Offer offer : offers) {
            if (offer == null) fail("There were null elements in list");
        }

        // hamcrest
        assertThat(offers, not(hasItem(nullValue())));

        // assertj
        assertThat(offers).doesNotContainNull();
    }

    @Test
    public void shouldNotContainNullsFailsHamcrest() {
        offers.add(null);
        assertThat(offers, not(hasItem(nullValue())));
    }

    @Test
    public void shouldNotContainNullsFailsAssertJ() {
        offers.add(null);
        assertThat(offers).doesNotContainNull();
    }

    @Test
    public void shouldContainOnlyAcceptedOffers() {
        offer1.accept();
        offer5.accept();

        // junit
        for (Offer offer : offers) {
            if (!offer.isOfferAccepted()) fail(String.format("This offer was not accepted %s", offer));
        }

        // hamcrest
        assertThat(offers, not(hasItem(hasProperty("offerAccepted", equalTo(false)))));

        // assertj
        assertThat(offers).allMatch(Offer::isOfferAccepted);
    }

    @Test
    public void shouldContainOnlyAcceptedOffersFailsJunit() {
        // negative of this approach is it will fail after finding the first element which does not match
        // while assertj will list all of them
        offer5.accept();

        for (Offer offer : offers) {
            if (!offer.isOfferAccepted())
                fail(String.format("Expected all offers to be accepted but this was not: %s", offer));
        }

        // if soft assertion is preferred this is and alternative implementation
        List<Offer> failedAssertions = new LinkedList<>();
        for (Offer offer : offers) {
            if (!offer.isOfferAccepted()) failedAssertions.add(offer);
        }

        if (!failedAssertions.isEmpty())
            fail(String.format("Expected all offers to be accepted but these were not: %s", failedAssertions));
    }

    @Test
    public void shouldContainOnlyAcceptedOffersFailsHamcrest() {
        offer5.accept();
        assertThat(offers, not(hasItem(hasProperty("offerAccepted", equalTo(false)))));
    }

    @Test
    public void shouldContainOnlyAcceptedOffersFailsAssertJ() {
        offer5.accept();
        assertThat(offers).allMatch(Offer::isOfferAccepted);
    }

    @Test
    public void otherAssertJExamples() {
//        assertThat(offers).hasOnlyElementsOfType(
//        assertThat(offers).doesNotHaveAnyElementsOfTypes(
//        assertThat(offers).haveAtMost(
//        assertThat(offers).endsWith(
    }
}