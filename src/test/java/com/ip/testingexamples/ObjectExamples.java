package com.ip.testingexamples;

import com.ip.testingexamples.domain.Email;
import com.ip.testingexamples.domain.Offer;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.hamcrest.core.CombinableMatcher.both;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsSame.sameInstance;
import static org.junit.Assert.assertSame;

public class ObjectExamples {
    private Offer expectedOffer;
    private Offer actualOffer;

    private Email expectedEmail;
    private Email actualEmail;

    @Before
    public void before() {
        expectedOffer = new Offer("cff47c9c-29ff-4742-9535-8cd8a2dfa90e", 1);
        actualOffer = new Offer("cff47c9c-29ff-4742-9535-8cd8a2dfa90e", 1);

        expectedEmail = new Email(expectedOffer);
        actualEmail = new Email(actualOffer);
    }

    // Equals

    @Test
    public void shouldCompareOffers() {
        // while JUnit can compare objects using equals only
        // Hamcrest and AssertJ offer comparison field by field what gives better description of unequal fields
        // This is especially helpful when objects don't have properly defined toString method
        // Moreover AssertJ can do so recursively

        // junit
        assertEquals(expectedOffer, actualOffer);

        // hamcrest
        assertThat(expectedOffer, equalTo(actualOffer));
        assertThat(expectedOffer, samePropertyValuesAs(actualOffer));

        // assertj
        assertThat(expectedOffer).isEqualTo(actualOffer);
        assertThat(expectedOffer).isEqualToComparingFieldByField(actualOffer);
        assertThat(expectedOffer).isEqualToComparingFieldByFieldRecursively(actualOffer);
    }

    @Test
    public void shouldCompareOffersFailsJunit() {
        actualOffer.accept();
        assertEquals(expectedOffer, actualOffer);
    }

    @Test
    public void shouldCompareOffersFailsHamcrest() {
        actualOffer.accept();

        assertThat(expectedOffer, equalTo(actualOffer));
        // java.lang.AssertionError:
        // Expected: <Offer{offerId='cff47c9c-29ff-4742-9535-8cd8a2dfa90e', offerAccepted=true, productId=1}>
        // but: was <Offer{offerId='cff47c9c-29ff-4742-9535-8cd8a2dfa90e', offerAccepted=false, productId=1}>
        // Expected :<Offer{offerId='cff47c9c-29ff-4742-9535-8cd8a2dfa90e', offerAccepted=true, productId=1}>
        //
        // Actual :<Offer{offerId='cff47c9c-29ff-4742-9535-8cd8a2dfa90e', offerAccepted=false, productId=1}>
        // <Click to see difference>

        assertThat(actualOffer, samePropertyValuesAs(expectedOffer));
        // java.lang.AssertionError:
        // Expected: same property values as Offer [offerAccepted: <true>, offerId: "cff47c9c-29ff-4742-9535-8cd8a2dfa90e", productId: <1>]
        // but: offerAccepted was <false>
    }

    @Test
    public void shouldCompareOffersFailsAssertJ() {
        actualOffer.accept();

        assertThat(expectedOffer).isEqualTo(actualOffer);
        // org.junit.ComparisonFailure:
        // Expected :Offer{offerId='cff47c9c-29ff-4742-9535-8cd8a2dfa90e', offerAccepted=true, productId=1}
        // Actual :Offer{offerId='cff47c9c-29ff-4742-9535-8cd8a2dfa90e', offerAccepted=false, productId=1}
        // <Click to see difference>

        assertThat(expectedOffer).isEqualToComparingFieldByField(actualOffer);
        // java.lang.AssertionError:
        // Expecting value <true> in field <"offerAccepted"> but was <false> in <Offer{offerId='cff47c9c-29ff-4742-9535-8cd8a2dfa90e', offerAccepted=false, productId=1}>.
        // Comparison was performed on all fields
    }

    // Same object verification

    @Test
    public void shouldCompareSameObject() {
        // junit
        assertSame(expectedOffer, expectedOffer);

        // hamcrest
        assertThat(expectedOffer, sameInstance(expectedOffer));

        // assertj
        assertThat(expectedOffer).isSameAs(expectedOffer);
    }

    @Test
    public void shouldCompareSameObjectFailsJunit() {
        // Yeah figure out what happened from this assertion error
        assertSame(expectedOffer, actualOffer);
        // java.lang.AssertionError: expected same:<Offer{offerId='cff47c9c-29ff-4742-9535-8cd8a2dfa90e', offerAccepted=false, productId=1}> was not:<Offer{offerId='5276F5ACFE000DCDE7858DDA26C', offerAccepted=false, productId=1}>
        // Expected :Offer{offerId='cff47c9c-29ff-4742-9535-8cd8a2dfa90e', offerAccepted=false, productId=1}
        // Actual :Offer{offerId='cff47c9c-29ff-4742-9535-8cd8a2dfa90e', offerAccepted=false, productId=1}
    }

    @Test
    public void shouldCompareSameObjectFailsHamcrest() {
        assertThat(actualOffer, sameInstance(expectedOffer));
    }

    @Test
    public void shouldCompareSameObjectFailsAssertJ() {
        assertThat(actualOffer).isSameAs(expectedOffer);
    }

    // Equals except some fields
    // - Email object has sendDate property which is initialised at object creation and has no setter

    @Test
    public void compareObjectsOmittingSomeFieldsJunit() {
        assertEquals(expectedEmail, actualEmail);
        // Possible workarounds:
        // 1. Assign the same value to a field which should not participate in comparison
        // - in current implementation only possible with reflection;
        // 2. Compare toString() of each object stripping out incomparable values with regular expression;
        // 3. Compare only fields we might possibly be interested in;
        // 4. Similar to 2. with custom toString - new ReflectionToStringBuilder(o, ToStringStyle.SHORT_PREFIX_STYLE)
        // .setExcludeFieldNames(new String[] { "sendDate" }).toString()
    }

    @Test
    public void compareObjectsOmittingSomeFieldsHamcrest() {
        // Hamcrest solution is not too much different from JUnit
        // except that field-by-field comparison might be implemented in one assert
        // chaining fields to compare with and()
        assertThat(expectedEmail,
                both(hasProperty("offer", equalTo(actualEmail.getOffer())))
                        .and(hasProperty("offer", equalTo(actualEmail.getOffer()))));
    }

    @Test
    public void compareObjectsOmittingSomeFieldsAssertJ() {
        assertThat(actualEmail).isEqualToIgnoringGivenFields(expectedEmail, "sendDate");
    }

    @Test
    public void compareObjectsOmittingSomeFieldsFailsAssertJ() {
        assertThat(actualEmail).isEqualToIgnoringGivenFields(expectedEmail, "offer");
        // java.lang.AssertionError:
        // Expecting value <2018-10-29T22:55:24.156> in field <"sendDate"> but was <2018-10-29T22:55:24.157> in <com.priceline.assertjexamples.domain.Email@14bf9759>.
        // Comparison was performed on all fields but <["offer"]>
    }
}